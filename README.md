Gwinnett County Personal Injury Lawyer
________________________________________________________________________________________________________________
I am David Brauns, the founding partner of Brauns Law. I only represent plaintiffs in injury cases and only handle personal injury claims. This allows me to focus solely on personal injury litigation and devote myself to helping injured residents in Duluth Georgia recover fair compensation for their damages.
________________________________________________________________________________________________________________
I began my legal career as an insurance defense attorney where I learned how insurance companies think and evaluate claims. I leveraged that insider�s knowledge and built a successful and powerful plaintiff�s practice. My commitment to customer service, transparency, and collaboration sets my practice apart from the majority of personal injury firms in the area.
________________________________________________________________________________________________________________
An Attorney with an Engineer�s Mind
________________________________________________________________________________________________________________
I did not start my professional life as an attorney, but rather as a software engineer. Even though I am an attorney now, I still tend to think like an engineer. I am process and quality driven. I am a Type-A person who will research something to death before making a decision. Coupled with my insurance company experience, this gives my clients an advantage when settling their claims. 
________________________________________________________________________________________________________________
Providing a Higher Standard of Service 
________________________________________________________________________________________________________________
I believe in helping clients with every aspect of their case, from getting their car fixed and getting them into a rental car to getting them the settlement they deserve. I also help clients get healthcare, even without insurance, as well as keep bill collectors at bay.
________________________________________________________________________________________________________________
This may sound like special treatment reserved for only the biggest cases, but the truth is that this is the standard for every client at Brauns Law.
________________________________________________________________________________________________________________
EDUCATION:
JD, Georgia State University, 2005,
BA, Clemson University, 1994
________________________________________________________________________________________________________________
EXPERIENCE:
Brauns Law, PC (Founding Attorney) 2008 - Present, 
Fain, Major & Brennan (Associate) 2006 - 2008, 
Hall, Booth, Smith & Slover, A Professional Corporation (Associate) 2004 - 2006
________________________________________________________________________________________________________________
ASSOCIATION:
Georgia Trial Lawyers Association, 
Forsyth County Bar Association, 
State Bar of Georgia, 
United States District Court for the Northern District of Georgia, 
Georgia Court of Appeals, 
Georgia Supreme Court
________________________________________________________________________________________________________________
AWARD:
Million Dollar Advocate (Million Dollar Advocates Forum) 2017, 
Multi-Million Dollar Advocate (Multi-Million Dollar Advocates Forum) 2017, 
Peer Review Rated (Martindale-Hubbell) 2017, 
Top 100 Trial Lawyers (National Trial Lawyers Association) 2016, 
Georgia Civil Justice Foundation Scholarship (Georgia State College of Law) 2004, 
Honors Litigation Workshop (Georgia State College of Law) 2004, 
________________________________________________________________________________________________________________
PUBLICATION:
Self Published Guide (Georgia Personal Injury Guide) 2008,
Self Published Guide (Georgia Personal Injury Guide) 2008 
Article, So You Want To Handle Your Own Case 
________________________________________________________________________________________________________________
COURTS ADMITTED FOR PRACTICE: 
All Georgia trial courts ,
Georgia Court of Appeals ,
Georgia Supreme Court ,
U.S. District Court, Northern District of Georgia 

________________________________________________________________________________________________________________
CONTACT:
Brauns Law Accident Injury Lawyers, PC, 
3175 Satellite Blvd Suite 330, 
Duluth, GA 30096, 
Phone: 678-329-9169